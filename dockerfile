FROM ubuntu:latest

RUN apt-get update
RUN apt-get install python3 -y 

COPY my_first_script.py ./

CMD [ "python3", "./my_first_script.py"]